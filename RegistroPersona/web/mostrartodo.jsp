<%@page import="java.sql.*"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="css/styles.css" rel="stylesheet" type="text/css"/> 
        <title>JSP Page</title>
    </head>
    <body style="margin-top: 30px; background: rgb(85, 48 ,93)">      
        <%

        Connection cone;
            String url = "jdbc:mysql://localhost:3306/recurso_humano";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String clave = "";
            Class.forName(Driver);
            cone = DriverManager.getConnection(url, user, clave);
            //Emnpezamos Listando los Datos de la Tabla Usuario
            Statement smt;
            ResultSet rs;
            smt = cone.createStatement();
            rs = smt.executeQuery("select * from tb_persona");
            //Creamo la Tabla:     
        %>
       
        <br>
                                   
           
                        
        <div class="container">     
            <a href="Agregar.jsp" class="btn btn-success" >Nuevo Registro </a>
            <br>
            <br>
            <table class="table table-bordered"  id="tablaDatos">
                    <thead>
                        <tr style="background: #a6e1ec">
                            <th class="text-center">ID</th>
                            <th class="text-center">DUI</th>
                            <th class="text-center">Apellidos</th>
                            <th class="text-center">Nombres</th>
                            <th class="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="tbodys">
                        <%
                            while (rs.next()) {
                        %>
                        <tr style="background: yellowgreen">
                            <td class="text-center"><%= rs.getInt("id")%></td>
                            <td ><%= rs.getString("dui_persona")%></td>
                            <td class="text-center"><%= rs.getString("apellidos_persona")%></td>
                            <td class="text-center"><%= rs.getString("nombre_persona")%></td>
                            <td class="text-center">
                                
                                
                                <a href="http://localhost:8080/RegistroPersona/editar.jsp?id=<%= rs.getInt("id")%>" class="btn btn-primary">Editar ó Actualizar</a>
                                <a href="http://localhost:8080/RegistroPersona/Delete.jsp?id=<%= rs.getInt("id")%>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                        <%}%>
                </table>
            </div>        
        
        </div>        
        <script src="js/jquery.js" type="text/javascript"></script>             
        <script src="js/bootstrap.min.js" type="text/javascript"></script>        
    </body>
</html>